console.log('helloworld');

//ES6 Updates

//1. Exponent Operator
//before

const firstNum = Math.pow(8,2);

//after

const secondNum = 8 ** 2;

//2. Template literals
//Allows to write strings without using the concatenation operator (+)
//Uses backtcks(``) ${expression}

const interestRate = .1;
const principal = 1000;

// console.log(`The interest on your savings account`)

//3. Array Destructuring
//Allows us to unpack elements in arrays into distinct variables
//Syntax:
    //let const [variableNameA,variableNameB,variableNameC]= array;

    const fullName = ["Juan","Dela","Cruz"];

    //Pre-array destructuring

    console.log(fullName[0])
    console.log(fullName[1])
    console.log(fullName[2])

   console.log(`Hello ${fullName[0]} ${fullName [1]} ${fullName [2]}! It's nice to meet you.`)

   //Array Destructuring

   const [firstName, middleName, lastName] = fullName;

   console.log(firstName);
   console.log(middleName);
   console.log(lastName);
  
   console.log(`Hello ${fullName[0]} ${fullName [1]} ${fullName [2]}! It's nice to meet you.`)


   const person = {
   	givenName: "Jane",
   	maidenName: "Dela",
   	familyName: "Cruz"
   }

   console.log(person.givenName)
   console.log(person.maidenName)
   console.log(person.familyName)


   console.log(`Hello ${person.givenName} ${person.maidenName} ${person.familyName}! It's good to see you again.`)

   //Object destructuring

   const{ givenName, maidenName,familyName} = person;

   console.log(givenName);
   console.log(maidenName);
   console.log(familyName);

   //shorten the syntax for accessing properties from objects

   function getFullName({ givenName, maidenName, familyName}){
   	console.log(`Hello ${givenName} ${maidenName} ${familyName}! It's good to see you`)
   }

   getFullName(person);

//traditional

function printFullname(firstName,middleName,lastName){
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullname("john", "Dela", "Cruz");

//arrow function
/*
Syntax:
	let/const variableName = (parameterA, parameterB) => {
		statement
	}

*/


let printFullname1 = (firstName,middleInitial,lastName) => {
	console.log(`${firstName} ${middleName} ${lastName}`)
}

printFullname("jane", "D", "Smith");

const students = ["Sally", "Kim", "Keeshia"];

//Arrow functions with loops
//Pre-arrow function
students.forEach(function(student) {
	console.log(`${student} is a student.`)
})

//Arrow Function
students.forEach((student) => {
	console.log(`${student} is a student.`)
})
//we can omit the paranthesis in arrow function if we only have one paramater.



//Implicit Return Statement
//There are instances when you can omit the "return" statement

//Pre - Arrow Function

// const add = (x, y) => {
// 	return x + y
// }

// let total = add(1, 2);
// console.log(total);


//Arrow Function
const add = (x, y) => x + y;

let total = add(1, 2);
console.log(total);


//Default Function Argument Value
//Provide a default argument value if none is provided when the function is invoked


const greet = (name = 'User') => {
	return `Good morning, ${name}!
`}

console.log(greet('Roldan'));


//Class-Based  Object blueprints

// function objectName() {
// 	this.keyA = keyA
// }

class Car {
	constructor(brand, name, year){
		this.brand = brand;
		this.name = name;
		this.year = year;
	}
}


const myCar = new Car();

console.log(myCar);

myCar.brand = "Ford";
myCar.name = "Ranger Raptor";
myCar.year = 2021;

console.log(myCar);


const myNewCar = new Car("Toyota", "Vios", 2021);

console.log(myNewCar);
